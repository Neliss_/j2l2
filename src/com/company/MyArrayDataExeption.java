package com.company;

public class MyArrayDataExeption extends Exception {
    private int line;
    private int coloumn;

    public MyArrayDataExeption(String message, int line, int coloumn) {
        super(message + "строка: " + (line+1) + ", столбец: " + (coloumn+1));
        this.line = line;
        this.coloumn = coloumn;

    }
}
