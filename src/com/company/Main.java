package com.company;

public class Main {

    public static void main(String[] args) throws MyArraySizeExeption, MyArrayDataExeption {

        System.out.println("Результат сложения элементов массива: " + sumNumsOfArray());

    }


    static String[][] array = new String[][]{{"0", "1", "2", "3"}, {"4", "5", "6", "7"}, {"8", "9", "10", "11"}, {"12", "13", "14", "15"}};

    static int sumNumsOfArray() throws MyArraySizeExeption, MyArrayDataExeption {
        int sum = 0;
        if (array.length != 4) throw new MyArraySizeExeption("Неправильный размер массива! Должен быть 4х4");
        for (int i = 0; i < array.length; i++) {
            if (array[i].length != 4) throw new MyArraySizeExeption("Неправильный размер массива! Должен быть 4х4");
            for (int j = 0; j < array[i].length; j++) {
                try {
                    int z = Integer.parseInt(array[i][j]);
                    sum = sum + z;
                } catch (NumberFormatException e) {
                    throw new MyArrayDataExeption("Не удалось преобразовать строку в число по адресу: ", i, j);


                }
            }
        }
        return sum;
    }


}


