package com.company;

public class MyArraySizeExeption extends Exception {

    public MyArraySizeExeption(String message) {
        super(message);
    }
}
